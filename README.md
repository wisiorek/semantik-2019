# Vorlesung "Logik und modelltheoretische Semantik"

#### Sommersemester 2019

*Centrum für Informations- und Sprachverarbeitung, LMU München*



## Termine:

Vorlesung: Di 14-16; L155 (Axel Wisiorek)

Tafelübung: Fr 10-12; L155 (Alena Moiseeva) 

Tutorium: Fr 12-14; L155 (Details siehe Moodle)


## Moodle-Übung:

https://moodle.lmu.de/course/view.php?id=3880


## Themen:

- **Einführung in die *Computational semantics***
- **Aussagen- und Prädikatenlogik**
- **Montague-Grammatik (Lambda-Kalkül)**
- ***Distributional semantics***





## Semesterplan:

Sitzung | Datum Vorlesung | Inhalt | Datum Übung  |
| ------------- | ------------- | ------------- | ------------- | 
| 0 | 23.4.19 | **Organisatorisches** / ***Semantik mit NLTK I*** (`notebook 0`) | 26.4.19 |
| 1 | 30.4.19 | **1. Formale Semantik** / ***Semantik mit NLTK II*** (`notebooks 1`) | 3.5.19 |
| 2 | 7.5.19 | **2. Lexikalische Semantik**  | 10.5.19 |
| 3 | 14.5.19  | **3. Aussagen und Argumente** | 17.5.19 |
| 4 | 21.5.19 | **4. Aussagenlogik** | 24.5.19 |
| 5 | 28.5.19 | **5. Prädikatenlogik** | 31.5.19 |
| 6 | 4.6.19 | **6. Wahrheitsbaumverfahren** | 7.6.19 |
| - | *- (Pfingstdienstag)* | *- (keine Vorlesung)* / Übung: PL, Baumkalkül (`notebooks 6`) | 14.6.19 |
| 7 | 18.6.19 | **7. Montague-Grammatik**  | 21.6.19 |
| 8 | 25.6.19 | **8. Distributional semantics I** | 28.6.19 |
| 9 | 2.7.19 | **9. Distributional semantics II** | 5.7.19 |
| 10 | 9.7.19 | **10. Wiederholung** (Themen, *`notebooks`*) / Übung: Probeklausur | 12.7.19 |
| 11 | 16.7.19 | 11: Klausurvorbereitung  / Übung: Besprechung Moodle-10 (Wdh.)  | 19.7.19 |
| - | 23.7.19 |   Klausur / Abschluss | 26.7.19 |


---
| Datum  | Klausur | 
| ------------- | ------------- | 
| **Di, 23.07.19** | Klausur (14-16 Uhr, Raum L155) | 

Möglichkeit zur Wiederholung erst zum nächstmöglichen regulären Termin im Sommersemster 2020.


## Literatur:

- **Beckermann, Ansgar (2011): Einführung in die Logik. Berlin/New York: de Gruyter** (als E-Book über UB)
- **Schwarz-Friesel, Monika & Jeannette Chur (1993; 4. Auflage 2004): Semantik. Ein Arbeitsbuch. Tübingen: Narr** (Einführung in linguistische und formale Semantik)
- **Carstensen, Kai-Uwe, Hrsg. (2010): Computerlinguistik Und Sprachtechnologie.**: Kapitel 2.1 (Logik, Lambda-Kalkül), 3.6 (Semantik); (als Ebook über UB)
- **Bird, Steven & Klein, Ewan & Loper, Edward (2009): Natural Language Processing with Python. Kapitel 10 (Analyzing the Meaning of Sentences).** http://www.nltk.org/book/ch10.html/